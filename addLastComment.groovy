import com.atlassian.jira.component.ComponentAccessor

def linkManager = ComponentAccessor.getIssueLinkManager()
def commentManager = ComponentAccessor.getCommentManager()
def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
//def issue = ComponentAccessor.getIssueManager().getIssueByCurrentKey("QA-2460")

def lastComment = ComponentAccessor.getCommentManager().getLastComment(issue).getBody()
def authorComment = commentManager.getLastComment(issue).getAuthorApplicationUser()
def assigneeIssue = issue.getAssignee()
def linkedIssue

linkManager.getInwardLinks(issue.id).each{    
	linkedIssue = it.sourceObject  
    def linkLastCommnet = commentManager.getLastComment(linkedIssue)
    if (linkLastCommnet == null){
        commentManager.create(linkedIssue,user,lastComment,true)
    } else {
        def bodyLinkLastComment = linkLastCommnet.getBody()
        if(lastComment != bodyLinkLastComment){
            commentManager.create(linkedIssue,user,lastComment,true)
        }
    }
} 