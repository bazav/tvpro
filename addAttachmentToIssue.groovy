import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.attachment.CreateAttachmentParamsBean
import com.atlassian.jira.issue.AttachmentManager
import com.onresolve.scriptrunner.db.DatabaseUtil
import groovy.sql.Sql 

//def issue = ComponentAccessor.getIssueManager().getIssueByCurrentKey("PP1-47252")
def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def lots = [15207,15214,15221,15228,15235]
String result
lots.each { 
    def cf = customFieldManager.getCustomFieldObject(it) 
    String valueCf = cf.getValue(issue)
    if(valueCf){
        def value = DatabaseUtil.withSql('KFSS') {


                it.eachRow(" SELECT LEVEL, ofc.offcnt_id, ofc.OFFCNT_NAME, omt.OFFCNT_MEDIA_TYPE_NAME,  t.MEDIA_PATH" +
                           " FROM ksas_dct.offer_content ofc" +
                           " JOIN ksas_dct.offcnt_media_content t ON ofc.OFFCNT_ID = t.OFFCNT_ID" +
                           " LEFT JOIN KSAS_DCC.OFFCNT_MEDIA_TYPE omt ON T.OFFCNT_MEDIA_TYPE_ID = OMT.OFFCNT_MEDIA_TYPE_ID" + 
                           " WHERE 1 = 1 AND LEVEL != 2 AND t.OFFCNT_MEDIA_TYPE_ID = -504" + 
                           " CONNECT BY PRIOR ofc.OFFCNT_ID =  ofc.OFFCNT_ID_PARENT" + 
                           " START WITH ofc.OFFCNT_ID IN ( SELECT DISTINCT oc.OFFCNT_ID" + 
                           " FROM KSAS_DCT.OFFER_CONTENT oc" +
                           " WHERE oc.OFFCNT_NAME LIKE '" + valueCf +"'" + 
                           " AND oc.OFFCNT_ID_PARENT IS NULL)" )
                {
                result = "${it.MEDIA_PATH}"
                }
        }
        
        String newResult = result.replace("\\", "/").substring(5)

        def newFile = new File("/mnt/Images/${newResult}")
        def attachmentManager = ComponentAccessor.getAttachmentManager()
        def bean = new CreateAttachmentParamsBean.Builder()

        bean.file(newFile)
        bean.filename(valueCf)
        bean.contentType("image/jpeg")
        bean.author(user)
        bean.issue(issue)
        bean.thumbnailable(true)
        bean.copySourceFile(true)

        attachmentManager.createAttachment(bean.build())
    }
} 