import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.security.roles.ProjectRoleManager
//https://coderoad.ru/36698657/%D0%9A%D0%B0%D0%BA-%D0%BF%D0%BE%D0%BB%D1%83%D1%87%D0%B8%D1%82%D1%8C-%D0%BF%D0%BE%D0%BB%D1%8C%D0%B7%D0%BE%D0%B2%D0%B0%D1%82%D0%B5%D0%BB%D1%8F-%D0%BF%D0%BE-email-%D0%B2-JIRA-Script-Runner
import com.atlassian.jira.user.*
import com.atlassian.jira.bc.user.search.UserSearchService
import com.atlassian.sal.api.user.UserManager
import javax.servlet.http.HttpServletRequest
import com.onresolve.scriptrunner.runner.rest.common.CustomEndpointDelegate
import groovy.transform.BaseScript
import groovy.json.JsonSlurper
import groovy.json.JsonBuilder

import javax.servlet.http.HttpServletRequest
import javax.ws.rs.core.MultivaluedMap
import javax.ws.rs.core.Response

@BaseScript CustomEndpointDelegate delegate

def newProject
getProjectkey(httpMethod: "GET", groups: ["jira-software-users"])
{ MultivaluedMap queryParams, String body, HttpServletRequest request ->

def userkey = request.getParameter("userkey")    
def projectManager = ComponentAccessor.getProjectManager()
def projectRoleManager = ComponentAccessor.getComponent(ProjectRoleManager)
    
def us = ComponentAccessor.getUserUtil().getUserByName(userkey)    
def prj =[]
def allProjects = projectManager.getProjectObjects()
allProjects.each{
    if(it.getKey() == "ELECTRONICS" || it.getKey() == "DECORATION" || it.getKey() == "SEASON_HOBBY" || it.getKey() == "SHOES" || it.getKey() == "CLOTHES" || it.getKey() == "MBT_CLEANING" || it.getKey() == "KITCHEN" || it.getKey() == "HEALTH" || it.getKey() == "CATALOG" || it.getKey() == "HOUSE" || it.getKey() == "BAY"){   
    	def projectRoles = projectRoleManager.getProjectRoles(us, it)
    	if(projectRoles.find(){it.getName() == "Users"}){
            newProject = it.getKey()
        } 
    }
}

return Response.ok(new JsonBuilder([projectkey:newProject]).toString()).build();

}
