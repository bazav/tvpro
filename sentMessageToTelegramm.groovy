import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.jql.parser.JqlQueryParser
import com.atlassian.jira.web.bean.PagerFilter

import com.atlassian.jira.component.ComponentAccessor
import groovyx.net.http.HTTPBuilder
import static groovyx.net.http.Method.POST
import static groovyx.net.http.ContentType.JSON

def jqlQueryParser = ComponentAccessor.getComponent(JqlQueryParser)
def searchService = ComponentAccessor.getComponent(SearchService)
def issueManager = ComponentAccessor.getIssueManager()
def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()

def query = jqlQueryParser.parseQuery('project in(IT-SUPPORT,WEB-SUPPORT,ITINFRA) AND "Time to first response" >= remaining("14m") AND "Time to first response" <= remaining("16m") AND status not in (Закрыто, "На подтверждении") and "Time to first response" = running()')

def search = searchService.search(user, query, PagerFilter.getUnlimitedFilter())
search.results.each {
    def newUserName = it.getAssignee().getDisplayName().replaceAll(" ", "+")
    def newSammary = it.getSummary().replaceAll(" ","+")
    def text = "*Внимание!*+Задача+[${it.getKey()}](https://jira.ru/browse/${it.getKey()})+${newSammary},+исполнитель:+${newUserName}!+SLA+истекает+через+15+минут"
    log.debug(it)
    def token = "000"
	def chatId = "000"
	def http = new HTTPBuilder( "https://api.telegram.org/bot${token}/sendMessage?chat_id=${chatId}&text=${text}&parse_mode=markdown" )
		http.request(POST, JSON ) { req ->    
	}
}