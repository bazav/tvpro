package Function
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.MutableIssue

class newIssue{
    static createIssue(String summary,
                           long project,
                           String issueTypeId,
                           String assingeeId,
                           String description,
                           String reporterId) {

        def issueFactory = ComponentAccessor.getIssueFactory()
        def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
        def issueManager = ComponentAccessor.getIssueManager()
        MutableIssue issue = issueFactory.getIssue()
        issue.setProjectId(project)
        issue.setIssueTypeId(issueTypeId)
        issue.setSummary(summary)
        issue.setAssigneeId(assingeeId)
        issue.setDescription(description)
        issue.setReporterId(reporterId)
        issueManager.createIssueObject(user, issue)

    }
}