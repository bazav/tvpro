import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.user.ApplicationUser
import com.onresolve.scriptrunner.db.DatabaseUtil
import groovy.sql.Sql 

import org.apache.log4j.Level
import org.apache.log4j.Logger

log = Logger.getLogger("com.acme.CreateFile")
log.setLevel(Level.DEBUG)

def ar =[]
def result = []
def issue = ComponentAccessor.getIssueManager().getIssueByCurrentKey("PP1-40187")
def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def cfm = customFieldManager.getCustomFieldObject(13001)
String lot_name = cfm.getValue(issue)
log.debug(lot_name)


def value = DatabaseUtil.withSql('KFSS') {
    it.call("{?=call ksas_exp.p\$alarm_card_2.f\$get_bayer(?)}", [Sql.VARCHAR,lot_name])    
    {
        result = "${it}"
    }
}
log.debug(result)