    import com.atlassian.jira.component.ComponentAccessor
    import com.atlassian.jira.issue.search.SearchRequest
    import com.atlassian.jira.issue.search.SearchRequestManager
    import com.atlassian.jira.favourites.FavouritesManager
    import com.atlassian.jira.user.ApplicationUser
    import com.atlassian.jira.portal.PortalPage
    import com.atlassian.jira.portal.PortalPageManager
    import com.atlassian.jira.favourites.FavouritesManager
    import com.atlassian.jira.user.util.UserUtil
    import com.atlassian.crowd.embedded.api.*
    // Set the portal page ID and group to share with here
        
    def portalPageIds = [13230]
    def groupNames = ['Арт оформление стратегические продюссеры']
    def groupManager = ComponentAccessor.getGroupManager()
    def srm = ComponentAccessor.getComponentOfType(SearchRequestManager.class)
    def portalPageManager = ComponentAccessor.getComponent(PortalPageManager)
    FavouritesManager favouritesManager = ComponentAccessor.getComponentOfType(FavouritesManager.class);

    for (String groupName: groupNames) {
    def grpUserList = groupManager.getUsersInGroup(groupName)
    for (ApplicationUser user:grpUserList){
         for (Long portalPageId: portalPageIds) {
              PortalPage portalPage = portalPageManager.getPortalPageById(portalPageId)
              try{
                 favouritesManager.addFavourite(user, portalPage) 
                 log.info("Adding portal " + portalPageId.toString() + " to user: " + user)
              } catch (Exception ex){
                 log.warn("Failed Adding portal " + portalPageId.toString() + " to user: " + user + "\n" + ex)
              } //catch 
         } //for portalID
    } //for group users
} //for groups